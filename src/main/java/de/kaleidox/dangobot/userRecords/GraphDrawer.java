package de.kaleidox.dangobot.userRecords;

import de.kaleidox.util.Utils;
import de.kaleidox.util.Value;
import de.kaleidox.util.serializer.PropertiesMapper;
import org.javacord.api.entity.message.Messageable;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.annotation.Nullable;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.MSG_COUNTS_LAST_WEEK;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.MSG_LENGTH_LAST_WEEK;

public class GraphDrawer {
    private static final ConcurrentHashMap<Server, GraphDrawer> selfMap = new ConcurrentHashMap<>();

    Server server;
    BufferedImage image;
    File imageFile;
    int maxWidth, maxHeight;
    UserRecordProcessor userRecordProcessor;

    GraphDrawer(Server srv) {
        server = srv;

        maxWidth = 800;
        maxHeight = 600;

        image = new BufferedImage(maxWidth, maxHeight, 1);
        // TODO https://www.tutorialspoint.com/jfreechart/jfreechart_bar_chart.htm https://www.tutorialspoint.com/jfreechart/jfreechart_line_chart.htm

        // retrieve image
        imageFile = new File("props/userRecords/" + srv.getId() + "/lastGraph.png");

        userRecordProcessor = UserRecordProcessor.softGet(server);
    }

    /**
     * This Object's "Constructor".
     * Looks for an already existing instance of the required Object or creates a new one.
     *
     * @return The adequate Instance of this Object.
     */
    public static GraphDrawer softGet(Server server) {
        GraphDrawer graphDrawer = selfMap.containsKey(server) ? selfMap.get(server) : selfMap.put(server, new GraphDrawer(server));

        if (graphDrawer == null) {
            return new GraphDrawer(server);
        } else {
            return graphDrawer;
        }
    }

    public GraphDrawer draw() {
        return draw(null);
    }

    public GraphDrawer draw(@Nullable User user) {
        PropertiesMapper entry = (user == null ? userRecordProcessor.getDefaultEntry() : userRecordProcessor.createOrGetUserEntry(user));
        ArrayList<Integer> weekCounts = Utils.trimListSize(7, 0, Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(entry), Value::asInteger));
        ArrayList<Integer> weekLength = Utils.trimListSize(7, 0, Utils.reformat(MSG_LENGTH_LAST_WEEK.getValuesFor(entry), Value::asInteger));

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(weekCounts.get(6), "Amount of Messages", "6 Days ago");
        dataset.addValue(weekCounts.get(5), "Amount of Messages", "5 Days ago");
        dataset.addValue(weekCounts.get(4), "Amount of Messages", "4 Days ago");
        dataset.addValue(weekCounts.get(3), "Amount of Messages", "3 Days ago");
        dataset.addValue(weekCounts.get(2), "Amount of Messages", "2 Days ago");
        dataset.addValue(weekCounts.get(1), "Amount of Messages", "1 Days ago");
        dataset.addValue(weekCounts.get(0), "Amount of Messages", "0 Days ago");

        dataset.addValue(weekLength.get(6), "Length of Messages", "6 Days ago");
        dataset.addValue(weekLength.get(5), "Length of Messages", "5 Days ago");
        dataset.addValue(weekLength.get(4), "Length of Messages", "4 Days ago");
        dataset.addValue(weekLength.get(3), "Length of Messages", "3 Days ago");
        dataset.addValue(weekLength.get(2), "Length of Messages", "2 Days ago");
        dataset.addValue(weekLength.get(1), "Length of Messages", "1 Days ago");
        dataset.addValue(weekLength.get(0), "Length of Messages", "0 Days ago");

        JFreeChart lineChartObject = ChartFactory.createLineChart(
                "Previous messages of " + (user == null ? server.getName() : user.getNickname(server).orElseGet(user::getName)),
                "Days before today",
                "Amount",
                dataset, PlotOrientation.VERTICAL,
                true, true, false);

        try {
            ChartUtilities.saveChartAsJPEG(imageFile, lineChartObject, maxWidth, maxHeight);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

    public void send(Messageable to) {
        to.sendMessage(imageFile);
    }

    public File getImage() {
        return imageFile;
    }
}
