package de.kaleidox.dangobot.userRecords;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.Debugger;
import de.kaleidox.util.ObjectVariableEnum;
import de.kaleidox.util.Utils;
import de.kaleidox.util.Value;
import de.kaleidox.util.serializer.PropertiesMapper;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.AVERAGE_MSG_LENGTH;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.AVG_MSG_PER_DAY;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.COUNTED_MSG_TODAY;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.MSG_COUNTS_LAST_WEEK;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.MSG_LENGTH_LAST_WEEK;
import static de.kaleidox.dangobot.userRecords.UserRecordProcessor.Variable.TOTAL_MSG_LENGTH;

public class UserRecordProcessor {
    private static final ConcurrentHashMap<Server, UserRecordProcessor> selfMap = new ConcurrentHashMap<>();

    private ConcurrentHashMap<User, PropertiesMapper> entries = new ConcurrentHashMap<>();
    private Debugger log = new Debugger(UserRecordProcessor.class.getName());
    private Server myServer;
    private Long serverId;
    private PropertiesMapper defaultEntry;

    private UserRecordProcessor(Server server) {
        this.myServer = server;

        serverId = myServer.getId();

        File folder = new File("props/userRecords/" + serverId + "/");
        folder.mkdirs();

        File defaultEntry = new File("props/userRecords/" + serverId + "/default.properties");

        if (!defaultEntry.exists()) {
            try {
                defaultEntry.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.defaultEntry = new PropertiesMapper(defaultEntry, ';');

        this.defaultEntry.add("average_msg_length", "0.0");
        this.defaultEntry.add("average_msg_per_day", "0.0");
        this.defaultEntry.add("counted_msg_today", "0");
        this.defaultEntry.add("total_msg_length", "0");

        Utils.safePut(selfMap, server, this);
    }

    /*
    This Object's "Constructor".
    Looks for an already existing instance of the required Object or creates a new one.

    @returns The adequate Instance of this Object.
     */
    public static UserRecordProcessor softGet(Server server) {
        return (selfMap.containsKey(server) ? selfMap.get(server) : selfMap.put(server, new UserRecordProcessor(server)));
    }

    public static void resetDailies() {
        selfMap.forEach((srv, proc) -> {
            proc.entries.forEach((user, mapper) -> {
                COUNTED_MSG_TODAY.setValueFor(mapper, 0);
                MSG_COUNTS_LAST_WEEK.addValueAtBeginning(mapper, 0);
                AVG_MSG_PER_DAY.setValueFor(mapper, 0);
                AVERAGE_MSG_LENGTH.setValueFor(mapper, 0);
                mapper.write();
            });
        });
    }

    public boolean decideDango(User usr, ServerTextChannel stc, DangoProcessor.LastDango lastDango) {
        int random = Utils.random(0, 100); // Creates a Random Number between 0 and 100
        double decision = 100; // Sets a deciding number to 100
        PropertiesMapper userEntry = createOrGetUserEntry(usr);
        Double msgLength = AVERAGE_MSG_LENGTH.getValueFor(userEntry).asDouble(); // Get the average message length of a user

        msgLength = msgLength / 40; // Divide the average message length by 40

        if (msgLength > 0)
            decision = decision * msgLength; // If the message length now is over 0, multiply it with the message length

        if (lastDango != null)
            if (lastDango.user.equals(usr))
                decision = decision - 0.2; // if you did get the last dango, 0.2 are removed from your theoretical chance of getting a dango

        if (stc.getMessagesAsStream()
                .limit(5)
                .anyMatch(m -> {
                    if (m.getUserAuthor().isPresent()) {
                        return m.getUserAuthor().get().equals(usr);
                    } else {
                        return false;
                    }
                })) {
            decision = decision / 4;
        } // basically if any of the last 5 messages in that channel is by you, your chance of getting a dango is quartered

        if (AVG_MSG_PER_DAY.getValueFor(userEntry).asDouble() < 20) {
            decision = decision * 3;
        } // if you send an average of 20 or less messages per day, you have a 3 times as high chance to get a dango

        return decision > random; // if your chance (decision) is higher than the random number, you get a dango
    }

    public void newMessage(MessageCreateEvent event) {
        Message msg = event.getMessage();
        int thisLength = msg.getContent().length();

        event.getMessage()
                .getUserAuthor()
                .ifPresent(usr -> {
                    event.getServer().ifPresent(server -> {
                        PropertiesMapper userEntry = createOrGetUserEntry(usr);
                        Integer todayCounted = COUNTED_MSG_TODAY.getValueFor(userEntry).asInteger();
                        Integer totalMsgLength = TOTAL_MSG_LENGTH.getValueFor(userEntry).asInteger();
                        ArrayList<Integer> weekCounts = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(userEntry), Value::asInteger);
                        ArrayList<Integer> weekLength = Utils.reformat(MSG_LENGTH_LAST_WEEK.getValuesFor(userEntry), Value::asInteger);

                        int newTotalMsgLength = totalMsgLength + thisLength;
                        Double newLenAvg = (double) Utils.addAllTogether(weekCounts) / (double) Utils.addAllTogether(weekLength);
                        Double newDayAvg = (double) Utils.addAllTogether(weekCounts) / (double) 7;

                        TOTAL_MSG_LENGTH.setValueFor(userEntry, newTotalMsgLength);
                        AVG_MSG_PER_DAY.setValueFor(userEntry, newDayAvg);
                        AVERAGE_MSG_LENGTH.setValueFor(userEntry, newLenAvg);
                        COUNTED_MSG_TODAY.setValueFor(userEntry, todayCounted + 1);
                        MSG_COUNTS_LAST_WEEK.changeValue(userEntry, 0, Utils.fromNullable(weekCounts, 0, 0) + 1);
                        MSG_LENGTH_LAST_WEEK.changeValue(userEntry, 0, Utils.fromNullable(weekLength, 0, 0) + thisLength);

                        userEntry.write();

                        Integer todayCountedS = COUNTED_MSG_TODAY.getValueFor(defaultEntry).asInteger();
                        Integer totalMsgLengthS = TOTAL_MSG_LENGTH.getValueFor(defaultEntry).asInteger();
                        ArrayList<Integer> weekCountsS = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(defaultEntry), Value::asInteger);
                        ArrayList<Integer> weekLengthS = Utils.reformat(MSG_LENGTH_LAST_WEEK.getValuesFor(defaultEntry), Value::asInteger);

                        int newTotalMsgLengthS = totalMsgLengthS + thisLength;
                        Double newLenAvgS = (double) Utils.addAllTogether(weekCountsS) / (double) Utils.addAllTogether(weekLengthS);
                        Double newDayAvgS = (double) Utils.addAllTogether(weekCountsS) / (double) 7;

                        TOTAL_MSG_LENGTH.setValueFor(defaultEntry, newTotalMsgLengthS);
                        AVG_MSG_PER_DAY.setValueFor(defaultEntry, newDayAvgS);
                        AVERAGE_MSG_LENGTH.setValueFor(defaultEntry, newLenAvgS);
                        COUNTED_MSG_TODAY.setValueFor(defaultEntry, todayCountedS + 1);
                        MSG_COUNTS_LAST_WEEK.changeValue(defaultEntry, 0, Utils.fromNullable(weekCountsS, 0, 0) + 1);
                        MSG_LENGTH_LAST_WEEK.changeValue(defaultEntry, 0, Utils.fromNullable(weekLengthS, 0, 0) + thisLength);
                    });
                });
    }

    public void sendInformation(ServerTextChannel stc, @Nullable User user, User requestedBy) {
        EmbedBuilder basicEmbed = DangoBot.getBasicEmbed(this.myServer, requestedBy);
        PropertiesMapper entry = (user == null ? this.getDefaultEntry() : this.createOrGetUserEntry(user));

        Double msgLength = AVERAGE_MSG_LENGTH.getValueFor(entry).asDouble();
        Double dayAverage = AVG_MSG_PER_DAY.getValueFor(entry).asDouble();
        Integer todayCounted = COUNTED_MSG_TODAY.getValueFor(entry).asInteger();
        Integer totalMsgLength = TOTAL_MSG_LENGTH.getValueFor(entry).asInteger();
        ArrayList<Integer> weekCounts = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(entry), Value::asInteger);
        ArrayList<Integer> weekLength = Utils.reformat(MSG_LENGTH_LAST_WEEK.getValuesFor(entry), Value::asInteger);

        basicEmbed
                .addField(AVERAGE_MSG_LENGTH.name, "```" + msgLength + "```")
                .addField(AVG_MSG_PER_DAY.name, "```" + dayAverage + "```")
                .addField(COUNTED_MSG_TODAY.name, "```" + todayCounted + "```")
                .addField(TOTAL_MSG_LENGTH.name, "```" + totalMsgLength + "```");

        for (int i = 0; i < weekCounts.size(); i++) {
            basicEmbed
                    .addInlineField(MSG_COUNTS_LAST_WEEK.name + " " + i + " Days ago:", "```" + weekCounts.get(i) + "```")
                    .addInlineField(MSG_LENGTH_LAST_WEEK.name + " " + i + " Days ago:", "```" + weekLength.get(i) + "```");
        }

        stc.sendMessage(basicEmbed);
    }

    PropertiesMapper createOrGetUserEntry(User user) {
        PropertiesMapper val = null;

        if (entries.containsKey(user)) {
            val = entries.getOrDefault(user, null);
        }

        if (val == null) {
            File thisEntry = new File("props/userRecords/" + serverId + "/" + user.getId() + ".properties");

            if (!thisEntry.exists()) {
                try {
                    thisEntry.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            val = new PropertiesMapper(thisEntry);
            entries.put(user, val);

            val.add("average_msg_length", "0.0");
            val.add("average_msg_per_day", "0.0");
            val.add("counted_msg_today", "0");
            val.add("total_msg_length", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_length_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.add("msg_counts_last_week", "0");
            val.write();
        }

        return val;
    }

    public GraphDrawer getGraphDrawer() {
        return GraphDrawer.softGet(myServer);
    }

    public PropertiesMapper getDefaultEntry() {
        return defaultEntry;
    }

    public enum Variable implements ObjectVariableEnum {
        AVERAGE_MSG_LENGTH("average_msg_length", 0, "0.0", Double.class),
        AVG_MSG_PER_DAY("average_msg_per_day", 0, "0.0", Double.class),
        COUNTED_MSG_TODAY("counted_msg_today", 0, "0", Integer.class),
        MSG_LENGTH_LAST_WEEK("msg_length_last_week", 0, "0", Integer.class) {
            public void addValueAtBeginning(PropertiesMapper mapper, int value) {
                ArrayList<Integer> weekCounts = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(mapper), Value::asInteger);
                weekCounts.remove(weekCounts.size() - 1);
                ArrayList<Integer> newWeekCounts = new ArrayList<>();
                newWeekCounts.add(value);
                newWeekCounts.addAll(weekCounts);

                MSG_LENGTH_LAST_WEEK.setValuesFor(mapper, newWeekCounts);
                mapper.write();
            }

            public void changeValue(PropertiesMapper mapper, int index, int value) {
                ArrayList<Integer> weekCounts = Utils.reformat(MSG_LENGTH_LAST_WEEK.getValuesFor(mapper), Value::asInteger);
                ArrayList<Integer> newWeekCounts = new ArrayList<>();

                for (int i = 0; i < 7; i++) {
                    if (i == index) {
                        newWeekCounts.add(value);
                    } else {
                        newWeekCounts.add(Utils.trimListSize(7, 0, weekCounts).get(i));
                    }
                }

                MSG_LENGTH_LAST_WEEK.setValuesFor(mapper, Utils.trimListSize(7, 0, newWeekCounts));
            }
        },
        MSG_COUNTS_LAST_WEEK("msg_counts_last_week", 0, "0", Integer.class) {
            public void addValueAtBeginning(PropertiesMapper mapper, int value) {
                ArrayList<Integer> weekCounts = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(mapper), Value::asInteger);
                weekCounts.remove(weekCounts.size() - 1);
                ArrayList<Integer> newWeekCounts = new ArrayList<>();
                newWeekCounts.add(value);
                newWeekCounts.addAll(weekCounts);

                MSG_COUNTS_LAST_WEEK.setValuesFor(mapper, newWeekCounts);
                mapper.write();
            }

            public void changeValue(PropertiesMapper mapper, int index, int value) {
                ArrayList<Integer> weekCounts = Utils.reformat(MSG_COUNTS_LAST_WEEK.getValuesFor(mapper), Value::asInteger);
                ArrayList<Integer> newWeekCounts = new ArrayList<>();

                for (int i = 0; i < 7; i++) {
                    if (i == index) {
                        newWeekCounts.add(value);
                    } else {
                        newWeekCounts.add(Utils.trimListSize(7, 0, weekCounts).get(i));
                    }
                }

                MSG_COUNTS_LAST_WEEK.setValuesFor(mapper, Utils.trimListSize(7, 0, newWeekCounts));
            }
        },
        TOTAL_MSG_LENGTH("total_msg_length", 0, "0", Integer.class);

        public String name;
        public int position;
        public String defaultValue;
        public Class type;

        Variable(String name, int position, String defaultValue, Class type) {
            this.name = name;
            this.position = position;
            this.defaultValue = defaultValue;
            this.type = type;
        }

        public static Optional<UserRecordProcessor.Variable> getVariable(String name) {
            return Arrays.stream(values())
                    .filter(v -> v.name.equals(name))
                    .findAny();
        }

        public Value getValueFor(PropertiesMapper mapper) {
            return new Value(mapper.softGet(this.name, this.position, this.defaultValue), this.type);
        }

        public ArrayList<Value> getValuesFor(PropertiesMapper mapper) {
            ArrayList<Value> reformat = Utils.reformat(mapper.getAll(this.name), t -> new Value(t, this.type));
            return reformat;
        }

        public <T> void setValueFor(PropertiesMapper mapper, T value) {
            setValueFor(mapper, new Value(value.toString(), value.getClass()));
        }

        public <T> void setValuesFor(PropertiesMapper mapper, ArrayList<T> values) {
            mapper.clear(this.name);
            mapper.addAll(this.name, Utils.reformat(values, Object::toString));
        }

        public void setValueFor(PropertiesMapper mapper, Value value) {
            mapper.set(this.name, this.position, value.asString());
        }

        public void addValueAtBeginning(PropertiesMapper mapper, int value) {
            throw new AbstractMethodError();
        }

        public void changeValue(PropertiesMapper mapper, int index, int value) {
            throw new AbstractMethodError();
        }
    }
}

