package de.kaleidox.dangobot.commands.advancedCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.Emoji;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.emoji.CustomEmoji;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class EmojiInteraction extends CommandBase {
    public EmojiInteraction() {
        super(new String[]{"emoji", "custom"}, true, false, new int[]{0, 1}, CommandGroup.ADVANCED_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Setup", "" +
                                "With this command, you can either see the current Emoji or define your own.\n" +
                                "The default emoji is \uD83C\uDF61.")
                        .addField("Usage Examples:", "" +
                                "`dango emoji` - Sends a message containing the current Emoji.\n" +
                                "`dango emoji :heart:` - Sets the emoji to ❤")
                        .setDescription("This command requires you to be an Authed User.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        if (param.size() == 0) {
            stc.sendMessage(DangoBot.getBasicEmbed(srv)
                    .addField("Current Emoji:", dangoProcessor.getEmoji().getPrintable())
            );
        } else if (param.size() == 1) {
            List<CustomEmoji> customEmojis = msg.getCustomEmojis();

            try {
                if (customEmojis.size() == 1) {
                    dangoProcessor.setEmoji(new Emoji(customEmojis.get(0)));
                } else if (customEmojis.size() == 0) {
                    dangoProcessor.setEmoji(new Emoji(param.get(0)));
                }
            } catch (NullPointerException e) {
                returnValue.set(SuccessState.ERRORED
                        .withMessage(e.getCause().getMessage(), "That Emoji is not from this Server. You must specify an emoji from this Server.")
                );
            } finally {
                stc.sendMessage(DangoBot.getBasicEmbed(srv)
                        .addField("New Emoji:", dangoProcessor.getEmoji().getPrintable())
                );

                returnValue.set(SuccessState.SUCCESSFUL);
            }
        } else {
            returnValue.set(SuccessState.ERRORED
                    .withMessage("Too many or too few arguments.")
            );
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
