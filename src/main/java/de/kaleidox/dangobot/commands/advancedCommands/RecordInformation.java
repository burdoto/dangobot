package de.kaleidox.dangobot.commands.advancedCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.userRecords.GraphDrawer;
import de.kaleidox.dangobot.userRecords.UserRecordProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class RecordInformation extends CommandBase {
    public RecordInformation() {
        super(new String[]{"records", "about"}, false, false, new int[]{0, 2}, CommandGroup.ADVANCED_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Record Information", "" +
                                "During his presence, the bot is keeping record on who sends how many messages per day and weeks, and how long those are.\n" +
                                "Every user can see their records upon using `dango records`, or even specify a user, like this: `dango records [User Mention]`\n" +
                                "The Recorded data is used to determine if a User is more likely to spam and therefore give him a lower chance of getting a Dango.\n" +
                                "For more information, please go to the Discord Server " + DangoBot.discordInvite() + " and talk to `" + DangoBot.OWNER_TAG + "`.")
                        .addField("NOTE:", "" +
                                "***The Bot does __NOT__ record any Message contents.***")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        User usr = msg.getUserAuthor().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        List<User> mentionedUsers = msg.getMentionedUsers();
        UserRecordProcessor userRecordProcessor = UserRecordProcessor.softGet(srv);

        if (param.size() == 2) {
            if (param.get(0).equals("graph")) {
                if (mentionedUsers.size() == 1) {
                    stc.sendMessage(GraphDrawer
                            .softGet(srv)
                            .draw(mentionedUsers
                                    .get(0))
                            .getImage()
                    );

                    returnValue.set(SuccessState.SUCCESSFUL);
                } else if (param.get(1).toLowerCase().equals("server")) {
                    stc.sendMessage(GraphDrawer
                            .softGet(srv)
                            .draw(null)
                            .getImage()
                    );

                    returnValue.set(SuccessState.SUCCESSFUL);
                } else {
                    stc.sendMessage(GraphDrawer
                            .softGet(srv)
                            .draw(mentionedUsers
                                    .get(0))
                            .getImage()
                    );

                    returnValue.set(SuccessState.SUCCESSFUL);
                }
            }
        } else if (param.size() == 1) {
            if (mentionedUsers.size() == 1) {
                if (param.get(0).equals("graph")) {
                    stc.sendMessage(GraphDrawer
                            .softGet(srv)
                            .draw(mentionedUsers
                                    .get(0))
                            .getImage()
                    );

                    returnValue.set(SuccessState.SUCCESSFUL);
                } else {
                    userRecordProcessor.sendInformation(stc, mentionedUsers.get(0), usr);

                    returnValue.set(SuccessState.SUCCESSFUL);
                }
            } else if (mentionedUsers.isEmpty()) {
                switch (param.get(0)) {
                    case "graph":
                        stc.sendMessage(GraphDrawer
                                .softGet(srv)
                                .draw(usr)
                                .getImage()
                        );

                        returnValue.set(SuccessState.SUCCESSFUL);
                        break;
                    case "server":
                        stc.sendMessage(GraphDrawer
                                .softGet(srv)
                                .draw(null)
                                .getImage()
                        );

                        returnValue.set(SuccessState.SUCCESSFUL);
                        break;
                    default:
                        userRecordProcessor.sendInformation(stc, usr, usr);

                        returnValue.set(SuccessState.SUCCESSFUL);
                        break;
                }
            } else {
                returnValue.set(SuccessState.ERRORED
                        .withMessage("No User found!")
                );
            }
        } else {
            if (param.get(0).equals("server")) {
                userRecordProcessor.sendInformation(stc, null, usr);
            } else {
                userRecordProcessor.sendInformation(stc, usr, usr);
            }

            returnValue.set(SuccessState.SUCCESSFUL);
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
