package de.kaleidox.dangobot.commands.advancedCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class CountInteraction extends CommandBase {
    public CountInteraction() {
        super(new String[]{"count", "per", "every"}, true, false, new int[]{0, 1}, CommandGroup.ADVANCED_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Setup", "" +
                                "With this command, you can either see after how many Messages a Dango is given or set a custom amount.\n" +
                                "The default value is 100.")
                        .addField("Usage Examples:", "" +
                                "`dango count` - Sends a message containing the current counter Maximum.\n" +
                                "`dango count 75` - Sets the counter maximum to 75")
                        .setDescription("This command requires you to be an Authed User.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        User usr = msg.getUserAuthor().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        if (param.size() == 0) {
            stc.sendMessage(DangoBot.getBasicEmbed(srv, usr)
                    .addField("Current Counter:", String.valueOf(dangoProcessor.getCounterMax()))
            );

            returnValue.set(SuccessState.SUCCESSFUL);
        } else if (param.size() == 1) {
            if (param.get(0).matches("[0-9]*")) {
                long val = Long.parseLong(param.get(0));
                if (val <= Integer.MAX_VALUE && val >= 25) {
                    dangoProcessor.setCounterMax(Integer.parseInt(param.get(0)));

                    stc.sendMessage(DangoBot.getBasicEmbed(srv, usr)
                            .addField("New Counter:", String.valueOf(dangoProcessor.getCounterMax()))
                    );

                    returnValue.set(SuccessState.SUCCESSFUL);
                } else if (val < 25) {
                    returnValue.set(SuccessState.ERRORED
                            .withMessage("The given Number is too small, needs to be greater than 25.")
                    );
                } else {
                    returnValue.set(SuccessState.ERRORED
                            .withMessage("The given Number is too big.")
                    );
                }
            }
        } else {
            returnValue.set(SuccessState.ERRORED
                    .withMessage("Too many or too few arguments.")
            );
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
