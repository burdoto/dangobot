package de.kaleidox.dangobot.commands.advancedCommands;

import de.kaleidox.Main;
import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.Utils;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.serializer.PropertiesMapper;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class LevelupActionInteraction extends CommandBase {
    public LevelupActionInteraction() {
        super(new String[]{"levelupaction", "action"}, true, false, new int[]{0, 3}, CommandGroup.ADVANCED_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Setup", "" +
                                "With this command, you can define custom Level-Actions, like applying a role to those on Level 10, or removing a role from those on Level 3\n" +
                                "There will be more Actions in the Future, if you want to request an action, use `dango discord` to get into the Bot Owner's Discord.")
                        .addField("Usage Examples:", "" +
                                "`dango action` - Lists all currently set Level actions.\n" +
                                "`dango action applyrole 6 @Regular` - Applies the role @Regular for the 6th level.\n" +
                                "`dango action delete 4 ` - Removes the REMOVEROLE Action from Level 4")
                        .addField("Be Careful!", "" +
                                "There are some things to note:\n" +
                                "\n" +
                                "- The Bot requires the proper Permissions to add a Role to an User or remove a Role from an User.\n" +
                                "- Be careful with this feature. _You don't usually want a User to get an Administrative Role through this Bot._\n" +
                                "- The Role that you are automatically giving the Users have to be mentionable. Currently you can only define Roles with their Mentions.")
                        .setDescription("This command requires you to be an Authed User.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);
        PropertiesMapper actions = dangoProcessor.actions;

        if (param.size() == 0) {
            EmbedBuilder embed = DangoBot.getBasicEmbed(srv)
                    .setDescription("Current Levelup-Actions:");
            StringBuilder field = new StringBuilder();

            if (!actions.entrySet().isEmpty()) {
                for (Map.Entry<String, List<String>> entry : actions.entrySet()) {
                    for (List<String> that : Utils.everyOfList(2, entry.getValue())) {
                        Optional<Role> roleById = Main.API.getRoleById(that.get(1));

                        switch (that.get(0)) {
                            case "applyrole":
                                field.append(roleById.map(role -> "Add Role: " + role.getMentionTag()).orElse("Unknown Role"));
                                field.append("\n");
                                break;
                            case "removerole":
                                field.append(roleById.map(role -> "Remove Role: " + role.getMentionTag()).orElse("Unknown Role"));
                                field.append("\n");
                                break;
                            case "adddango":
                            default:
                                field.append("Add Dango");
                                field.append("\n");
                                break;
                        }
                    }

                    embed.addField("Level " + entry.getKey() + ":", field.toString());
                    field = new StringBuilder();
                }
            } else {
                embed.addField("Whoops!", "There are currently no LevelUp-Actions.");
            }

            stc.sendMessage(embed);
        } else if (param.size() >= 2 && param.size() <= 3) {
            if (param.get(1).matches("[0-9]+")) {
                int level = Integer.parseInt(param.get(1));
                List<Role> mentionedRoles = msg.getMentionedRoles();
                Role role;
                Optional<Role> roleOpt = mentionedRoles.stream()
                        .limit(1)
                        .findAny();

                switch (param.get(0)) {
                    case "applyrole":
                        if (roleOpt.isPresent()) {
                            role = roleOpt.get();

                            if (actions.mapSize() < 25 && !actions.containsValue(level, role.getId())) {
                                dangoProcessor.addRoleAction(level, "applyrole", role);
                            } else if (!actions.containsValue(level, role.getId())) {
                                returnValue.set(SuccessState.ERRORED
                                        .withMessage("There is already an Action with this Role!")
                                );
                            } else {
                                returnValue.set(SuccessState.ERRORED
                                        .withMessage("There are already too many Actions!")
                                );
                            }
                        } else {
                            returnValue.set(SuccessState.ERRORED
                                    .withMessage("No Role Found.")
                            );
                        }

                        break;
                    case "removerole":
                        if (roleOpt.isPresent()) {
                            role = roleOpt.get();

                            if (actions.mapSize() < 25 && !actions.containsValue(level, role.getId())) {
                                dangoProcessor.addRoleAction(level, "removerole", role);
                            } else if (!actions.containsValue(level, role.getId())) {
                                returnValue.set(SuccessState.ERRORED
                                        .withMessage("There is already an Action with this Role!")
                                );
                            } else {
                                returnValue.set(SuccessState.ERRORED
                                        .withMessage("There are already too many Actions!")
                                );
                            }
                        } else {
                            returnValue.set(SuccessState.ERRORED
                                    .withMessage("No Role Found.")
                            );
                        }

                        break;
                    case "delete":
                        if (param.size() == 3) {
                            dangoProcessor.removeAction(level, param.get(2));
                        } else {
                            dangoProcessor.removeActions(level);
                        }
                        break;
                }
            } else {
                returnValue.set(SuccessState.ERRORED
                        .withMessage("Too many or too few arguments, or wrong argument order.",
                                "The correct use is:\n" +
                                        "`dango action <Actiontitle> <Level> [Parameter]` or `dango action delete <Level> <Actiontitle>`\n" +
                                        "\n" +
                                        "Examples:\n" +
                                        "`dango action applyrole 6 @Regular` - Applies the role @Regular for the 6th level.\n" +
                                        "`dango action delete 4 ` - Removes the REMOVEROLE Action from Level 4")
                );
            }
        } else {
            returnValue.set(SuccessState.ERRORED
                    .withMessage("Too many or too few arguments.",
                            "The correct use is:\n" +
                                    "`dango action <Actiontitle> <Level> [Parameter]` or `dango action delete <Level> <Actiontitle>`\n" +
                                    "\n" +
                                    "Examples:\n" +
                                    "`dango action applyrole 6 @Regular` - Applies the role @Regular for the 6th level.\n" +
                                    "`dango action delete 4 ` - Removes the REMOVEROLE Action from Level 4")
            );
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
