package de.kaleidox.dangobot.commands.mainCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class Scoreboard extends CommandBase {
    public Scoreboard() {
        super(new String[]{"stats", "scores", "score", "scoreboard"}, false, false, new int[]{0, 0}, CommandGroup.MAIN_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Commands", "" +
                                "Sends a detailed List of all the Users that have Dangos and their Scores on this Server.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        Message msg = event.getMessage();
        Server srv = event.getServer().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        return dangoProcessor.sendScoreboard(stc, false);
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
