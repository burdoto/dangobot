package de.kaleidox.dangobot.commands.mainCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.discord.ui.Response;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class Revoke extends CommandBase {
    public Revoke() {
        super("revoke", true, false, new int[]{0, 1}, CommandGroup.MAIN_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Commands", "" +
                                "With this command you can undo the last given Dango.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = event.getServer().get();
        User usr = msg.getUserAuthor().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        if (param.size() == 1) {
            if (Boolean.valueOf(param.get(0))) {
                dangoProcessor.revokeDango();
            }
        } else {
            Response.areYouSure(stc, usr, "Are you sure?", "Do you really want to revoke the last Dango?", 30, TimeUnit.SECONDS)
                    .thenAcceptAsync(yesno -> {
                        if (yesno) {
                            if (dangoProcessor.lastDango != null) {
                                dangoProcessor.revokeDango();

                                returnValue.set(SuccessState.SUCCESSFUL);
                            } else {
                                returnValue.set(SuccessState.ERRORED
                                        .withMessage("There is no Dango to revoke.")
                                );
                            }
                        }
                    });
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
