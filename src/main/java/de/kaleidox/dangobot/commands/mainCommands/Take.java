package de.kaleidox.dangobot.commands.mainCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Take extends CommandBase {
    public Take() {
        super(new String[]{"take", "remove"}, true, false, new int[]{1, 2}, CommandGroup.MAIN_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Commands", "" +
                                "With this command you can take Dangos from specified users.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);
        List<User> mentionedUsers = msg.getMentionedUsers();

        if (mentionedUsers.size() == 1) {
            User user = mentionedUsers.get(0);

            if (param.size() == 2) {
                String s = param.get(1);

                if (s.matches("[0-9]+")) {
                    int i = Integer.parseInt(s);
                    dangoProcessor.removeDango(user, stc, i);

                    returnValue.set(SuccessState.SUCCESSFUL);
                } else {
                    dangoProcessor.removeDango(user, stc, 1);

                    returnValue.set(SuccessState.SUCCESSFUL
                            .withMessage("You did not specify a valid number, so " + user.getNickname(srv).orElseGet(user::getName) + " lost 1 Dango.")
                    );
                }
            } else {
                dangoProcessor.removeDango(user, stc, 1);

                returnValue.set(SuccessState.SUCCESSFUL
                        .withMessage("You did not specify a number, so " + user.getNickname(srv).orElseGet(user::getName) + " lost 1 Dango.")
                );
            }
        } else {
            returnValue.set(SuccessState.ERRORED
                    .withMessage("No user found", "You didn't mention an User!")
            );
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
