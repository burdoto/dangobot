package de.kaleidox.dangobot.commands.mainCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;

public class SelfStats extends CommandBase {
    public SelfStats() {
        super(new String[]{"self", "own", "my", "mine"}, false, false, new int[]{0, 0}, CommandGroup.MAIN_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Commands", "" +
                                "Sends how many Dangos you have got.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        Server srv = event.getServer().get();
        User usr = event.getMessage().getUserAuthor().get();
        ServerTextChannel stc = event.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        dangoProcessor.sendUserScore(stc, usr);

        return SuccessState.SILENT;
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
