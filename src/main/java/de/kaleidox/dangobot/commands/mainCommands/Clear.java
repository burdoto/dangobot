package de.kaleidox.dangobot.commands.mainCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.dangobot.DangoProcessor;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.discord.ui.Response;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Clear extends CommandBase {
    public Clear() {
        super(new String[]{"reset", "clear"}, true, false, new int[]{0, 0}, CommandGroup.MAIN_COMMANDS,
                DangoBot.getBasicEmbed()
                        .addField("Dango Commands", "" +
                                "With this command you can reset the complete Dango Process on the server.\n" +
                                "Before the reset will happen, you are being asked in Chat if you are sure if you want to delete all progress.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        Server srv = event.getServer().get();
        User usr = event.getMessage().getUserAuthor().get();
        ServerTextChannel stc = event.getServerTextChannel().get();
        DangoProcessor dangoProcessor = DangoProcessor.softGet(srv);

        Response.areYouSure(stc, usr, "Are you sure?", "Do you really want to reset this server's scores?", 30, TimeUnit.SECONDS)
                .thenAcceptAsync(yesno -> {
                    if (yesno) {
                        dangoProcessor.clearAll();
                    }
                });

        return SuccessState.SILENT;
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return null;
    }
}
