package de.kaleidox.dangobot;

import de.kaleidox.Main;
import de.kaleidox.util.Bot;
import de.kaleidox.util.Utils;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DangoBot extends Bot {
    public static String VERSION_NUMBER = "1.11.5";
    public static boolean isTesting = System.getProperty("os.name").equals("Windows 10");
    public static Long BOT_ID = 439082176537952267L;
    public static Long DBL_BOT_ID = BOT_ID;
    public static String BOT_URL = "http://dangobot.kaleidox.de/";
    public static String ICON_URL = "http://wppullzone1.epicmatcha.netdna-cdn.com/wp-content/uploads/2016/03/matcha-cookies-hanami-dango.jpg";
    public static String OWNER_TAG = "@Kaleidox#0001";
    public static Long PERMISSION_STRING = 470248512L;
    public static String INVITE_LINK = "https://discordapp.com/oauth2/authorize?client_id=" + BOT_ID + "&scope=bot&permissions=" + PERMISSION_STRING;
    public static HashSet<String> KEYWORDS = new HashSet<String>() {{
        add("dango");
        add("dangobot");
        add(Main.SELF.getMentionTag());
        add(Main.SELF.getNicknameMentionTag());
    }};

    public static EmbedBuilder getBasicEmbed(Server forServer, User forUser) {
        java.util.List<Color> collect = forServer.getRolesOf(Main.SELF)
                .stream()
                .map(Role::getColor)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        return new EmbedBuilder()
                .setFooter("Requested by " + forUser.getDiscriminatedName(), forUser.getAvatar().getUrl().toString())
                .setAuthor(botName(), BOT_URL, ICON_URL)
                .setTimestamp()
                .setUrl(BOT_URL)
                .setColor(collect.get(collect.size() - 1)
                );
    }

    public static EmbedBuilder getBasicEmbed(Server forServer) {
        List<Color> collect = forServer.getRolesOf(Main.SELF)
                .stream()
                .map(Role::getColor)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());

        return new EmbedBuilder()
                .setFooter(botName() + " by " + OWNER_TAG)
                .setAuthor(botName(), BOT_URL, ICON_URL)
                .setTimestamp()
                .setUrl(BOT_URL)
                .setColor(collect.get(collect.size() - 1)
                );
    }

    public static EmbedBuilder getBasicEmbed() {
        return new EmbedBuilder()
                .setFooter(botName() + " by " + OWNER_TAG)
                .setAuthor(botName(), BOT_URL, ICON_URL)
                .setTimestamp()
                .setUrl(BOT_URL)
                .setColor(Utils.getRandomColor()
                );
    }
}
