package de.kaleidox.util.commands.genericBotCommands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.util.ServerPreferences;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import de.kaleidox.util.discord.ui.Response;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class Preferences extends CommandBase {
    public Preferences() {
        super(new String[]{"setup", "pref", "preferences"}, true, false, new int[]{0, 2}, CommandGroup.BOT_SETUP,
                EmbedMaker.getBasicEmbed()
                        .addField("Bot Preferences", "" +
                                "Lets you specify how you want the bot to behave in different ways.")
                        .addField("Changing Values", "" +
                                "If you want to change a value of a variable, you need to provide a fitting value for that variable.\n" +
                                "The Variables have different Regular Expressions that define what kind of Inputs are valid, and if you click the reactions from DangoBot on your Commands, you get a detailed description on what went wrong, or if everything went alright.")
                        .addField("Resetting to defaults:", "" +
                                "The preferences can be set to their defaults by using `dango setup reset`")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        ServerTextChannel stc = msg.getServerTextChannel().get();
        User usr = msg.getUserAuthor().get();
        ServerPreferences serverPreferences = ServerPreferences.softGet(srv);

        if (param.size() == 0) {
            // post preferences
            EmbedBuilder basicEmbed = DangoBot.getBasicEmbed(srv);

            Arrays.asList(ServerPreferences.Variable.values())
                    .forEach(variable -> {
                        basicEmbed.addField("" +
                                variable.name, "```" +
                                serverPreferences.get(variable).asString() + "```");
                    });

            stc.sendMessage(basicEmbed);
        } else if (param.size() == 1) {
            // only one parameter
            if (param.get(0).toLowerCase().equals("reset")) {
                Response.areYouSure(stc, usr, "Are you sure?", "Do you really want to reset all the server's preferences to their default values?", 30, TimeUnit.SECONDS)
                        .thenAcceptAsync(yesno -> {
                            if (yesno) {
                                serverPreferences.resetAll();

                                returnValue.set(SuccessState.SUCCESSFUL);
                            }
                        });
            } else {
                StringBuilder variables = new StringBuilder();
                Arrays.asList(ServerPreferences.Variable.values())
                        .forEach(variable -> {
                            variables
                                    .append("- `")
                                    .append(variable.name)
                                    .append("`\n");
                        });

                returnValue.set(SuccessState.ERRORED
                        .withMessage("Not enough or too many Arguments!", "Possible Variables are:\n" + variables.substring(0, variables.length() - 1))
                );
            }
        } else {
            // edit preferences
            if (param.size() == 2) {
                Optional<ServerPreferences.Variable> variableOptional = ServerPreferences.Variable.getVariable(param.get(0));

                if (variableOptional.isPresent()) {
                    if (param.get(1).toLowerCase().equals("reset")) {
                        // reset the value
                        ServerPreferences.Variable variable = variableOptional.get();

                        serverPreferences.reset(variable);

                        returnValue.set(SuccessState.SUCCESSFUL);
                    } else {
                        ServerPreferences.Variable variable = variableOptional.get();

                        returnValue.set(
                                serverPreferences.set(variable, param.get(1))
                        );
                    }
                } else {
                    StringBuilder variables = new StringBuilder();
                    Arrays.asList(ServerPreferences.Variable.values())
                            .forEach(variable -> {
                                variables
                                        .append("- `")
                                        .append(variable.name)
                                        .append("`\n");
                            });

                    returnValue.set(SuccessState.ERRORED
                            .withMessage("Unknown Variable Name!", "Possible Variables are:\n" + variables.substring(0, variables.length() - 1))
                    );
                }

            } else {
                StringBuilder variables = new StringBuilder();
                Arrays.asList(ServerPreferences.Variable.values())
                        .forEach(variable -> {
                            variables
                                    .append("- `")
                                    .append(variable.name)
                                    .append("`\n");
                        });

                returnValue.set(SuccessState.ERRORED
                        .withMessage("Not enough or too many Arguments!", "Possible Variables are:\n" + variables.substring(0, variables.length() - 1))
                );
            }
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return SuccessState.UNIMPLEMENTED;
    }
}
