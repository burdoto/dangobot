package de.kaleidox.util.commands.genericBotCommands;

import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.Optional;

public class Testcommand extends CommandBase {
    public Testcommand() {
        super("testcommand", false, true, new int[]{0, 1}, CommandGroup.BOT_SETUP,
                EmbedMaker.getBasicEmbed()
                        .addField("you should not be using this", "" +
                                ":P")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        Optional<User> optionalUser = event.getMessage().getUserAuthor();
        Server srv = event.getServer().get();
        ServerTextChannel stc = event.getServerTextChannel().get();

        if (optionalUser.isPresent()) {
            User usr = optionalUser.get();

            if (usr.isBotOwner()) {
                return SuccessState.SUCCESSFUL.withMessage("The cake is a lie.");
            }
        }

        return SuccessState.UNAUTHORIZED;
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        Optional<User> optionalUser = event.getMessage().getUserAuthor();

        if (optionalUser.isPresent()) {
            User usr = optionalUser.get();

            if (usr.isBotOwner()) {
                event.getServerTextChannel().get();

                return SuccessState.SUCCESSFUL.withMessage("The cake is a lie.");
            }
        }

        return SuccessState.UNAUTHORIZED;
    }
}
