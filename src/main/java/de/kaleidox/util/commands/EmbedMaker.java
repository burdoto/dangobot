package de.kaleidox.util.commands;

import de.kaleidox.util.Bot;
import de.kaleidox.util.Utils;
import org.javacord.api.Javacord;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import static de.kaleidox.util.Bot.botName;

public class EmbedMaker {
    private static final String loading = "[_Loading..._]";

    public static String inviteLink = loading;
    public static String donateLink = loading;
    public static String discordLink = loading;
    public static String gitLink = loading;
    public static String bugreportLink = loading;
    public static String version = loading;

    public static void init(String pVersion, String pInviteLink, String pDonateLink, String pDiscordLink, String pGitLink, String pBugreportLink) {
        version = pVersion;
        inviteLink = pInviteLink;
        donateLink = pDonateLink;
        discordLink = pDiscordLink;
        gitLink = pGitLink;
        bugreportLink = pBugreportLink;
    }

    public static EmbedBuilder getBasicEmbed() {
        return new EmbedBuilder()
                .setFooter(botName())
                .setAuthor(botName())
                .setTimestamp()
                .setColor(Utils.getRandomColor()
                );
    }

    public static EmbedBuilder bugreport() {
        return getBasicEmbed()
                .addField("Report Bugs over here:", bugreportLink);
    }

    public static EmbedBuilder discord() {
        return getBasicEmbed()
                .addField("Our Discord Server:", discordLink);
    }

    public static EmbedBuilder donate() {
        return getBasicEmbed()
                .addField("You can Donate for this Bot here, if you want:", donateLink);
    }

    public static EmbedBuilder help() {
        return getBasicEmbed()
                .addField("Help", "" +
                        "Sends a list of all commands");
    }

    public static EmbedBuilder info() {
        return getBasicEmbed()
                .addField("About the bot:", "" +
                        "Running on Version " + version + "\n" +
                        "Made with love by " + Bot.ownerTag() + "\n" +
                        "GitHub: " + gitLink + "\n" +
                        "Running on Javacord Version " + Javacord.VERSION + "\n" +
                        "\n" +
                        "Enjoy!");
    }

    public static EmbedBuilder invite() {
        return getBasicEmbed()
                .addField("The Invitation for the Bot:", inviteLink);
    }

    /**

     HELP(DangoBot.getBasicEmbed()
     .addField("Help", "" +
     "Sends a list of all commands")
     ),
     INFO(DangoBot.getBasicEmbed()
     .addField("About the bot:", "" +
     "Running on Version " + DangoBot.VERSION_NUMBER + "\n" +
     "Made with love by " + DangoBot.OWNER_TAG + " in 2018 with help of <@268818940384247819>\n" +
     "GitHub: https://github.com/Kaleidox00/DangoBot\n" +
     "Running on Javacord Version " + Javacord.VERSION + "\n" +
     "\n" +
     "Enjoy!")
     ),
     BUGREPORT(DangoBot.getBasicEmbed()
     .addField("Report Bugs over here:", "" +
     "https://github.com/Kaleidox00/Steve/issues")
     ),
     INVITE(DangoBot.getBasicEmbed()
     .addField("The Invitation for the Bot:", DangoBot.INVITE_LINK)
     ),
     DISCORD(DangoBot.getBasicEmbed()
     .addField("Our Discord Server:", Bot.discordInvite())
     ),
     DONATE(DangoBot.getBasicEmbed()
     .addField("You can Donate for this Bot here, if you want:", "" +
     "http://donate.kaleidox.de/")
     );

     */
}
