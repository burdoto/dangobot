package de.kaleidox.util.commands.baseCommands;

import de.kaleidox.util.SuccessState;
import de.kaleidox.util.Utils;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import de.kaleidox.util.discord.messages.PagedMessage;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class Help extends CommandBase {
    public Help() {
        super("help", false, true, new int[]{0, 1}, CommandGroup.BASIC, EmbedMaker.help());
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        ServerTextChannel stc = msg.getServerTextChannel().get();

        if (param.size() == 0) {
            PagedMessage.get(stc, () -> "**All Commands:**\n",
                    () -> {
                        StringBuilder sb = new StringBuilder(), cmdText;
                        CommandGroup lastGroup = CommandGroup.NONE;

                        for (CommandBase command : CommandBase.commands) {
                            cmdText = new StringBuilder();

                            cmdText
                                    .append("Command Keywords: `")
                                    .append(Utils.concatStrings(" / ", command.keywords))
                                    .append("` | Private Chat: ")
                                    .append(command.canRunPrivately ? "✅" : "❌")
                                    .append(" | Requires Authorization: ")
                                    .append(command.requiresAuth ? "✅" : "❌")
                                    .append(" | Supported Parameter: ")
                                    .append(command.serverParameterRange[0])
                                    .append(" up to ")
                                    .append(command.serverParameterRange[1])
                                    .append("\n");

                            if (!lastGroup.equals(command.group)) {
                                sb.append(command.group.name)
                                        .append(cmdText.toString());

                                lastGroup = command.group;
                            }
                        }

                        return sb.substring(0, sb.length() - 1);
                    });

            returnValue.set(SuccessState.SUCCESSFUL);
        } else {
            Optional<CommandBase> cmdOpt = findCommand(param.get(0));

            if (cmdOpt.isPresent()) {
                CommandBase command = cmdOpt.get();

                stc.sendMessage(command.helpEmbed);

                returnValue.set(SuccessState.SUCCESSFUL);
            } else {
                returnValue.set(SuccessState.ERRORED
                        .withMessage("Command not found.")
                );
            }
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        User usr = msg.getUserAuthor().get();

        if (param.size() == 0) {
            PagedMessage.get(usr, () -> "**All Commands:**\n",
                    () -> {
                        StringBuilder sb = new StringBuilder(), cmdText;
                        CommandGroup lastGroup = CommandGroup.NONE;

                        for (CommandBase command : CommandBase.commands) {
                            cmdText = new StringBuilder();

                            cmdText
                                    .append("Command Keywords: `")
                                    .append(Utils.concatStrings(" / ", command.keywords))
                                    .append("` | Private Chat: ")
                                    .append(command.canRunPrivately ? "✅" : "❌")
                                    .append(" | Requires Authorization: ")
                                    .append(command.requiresAuth ? "✅" : "❌")
                                    .append(" | Supported Parameter: ")
                                    .append(command.serverParameterRange[0])
                                    .append(" up to ")
                                    .append(command.serverParameterRange[1])
                                    .append("\n");

                            if (!lastGroup.equals(command.group)) {
                                sb.append(command.group.name)
                                        .append(cmdText.toString());

                                lastGroup = command.group;
                            }
                        }

                        return sb.substring(0, sb.length() - 1);
                    });

            returnValue.set(SuccessState.SUCCESSFUL);
        } else {
            Optional<CommandBase> cmdOpt = findCommand(param.get(0));

            if (cmdOpt.isPresent()) {
                CommandBase command = cmdOpt.get();

                usr.sendMessage(command.helpEmbed);

                returnValue.set(SuccessState.SUCCESSFUL);
            } else {
                returnValue.set(SuccessState.ERRORED
                        .withMessage("Command not found.")
                );
            }
        }

        return returnValue.get();
    }
}
