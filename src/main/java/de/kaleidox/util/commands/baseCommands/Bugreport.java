package de.kaleidox.util.commands.baseCommands;

import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Bugreport extends CommandBase {
    public Bugreport() {
        super(new String[]{"bug", "bugreport"}, false, true, new int[]{0, 1}, CommandGroup.BASIC, EmbedMaker.bugreport());
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();

        msg.getServerTextChannel()
                .ifPresent(serverTextChannel -> {
                    serverTextChannel.sendMessage(EmbedMaker.bugreport());

                    returnValue.set(SuccessState.SUCCESSFUL);
                });

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();

        msg.getUserAuthor()
                .ifPresent(user -> {
                    user.sendMessage(EmbedMaker.bugreport());

                    returnValue.set(SuccessState.SUCCESSFUL);
                });

        return returnValue.get();
    }
}
