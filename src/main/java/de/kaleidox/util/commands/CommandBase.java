package de.kaleidox.util.commands;

import de.kaleidox.dangobot.DangoBot;
import de.kaleidox.util.Auth;
import de.kaleidox.util.Debugger;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.baseCommands.Help;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class CommandBase {
    public static List<CommandBase> commands = new ArrayList<>();
    private static Debugger log = new Debugger(CommandBase.class.getName());

    static {
        new Reflections(CommandBase.class.getPackage().getName())
                .getSubTypesOf(CommandBase.class)
                .forEach(cl -> {
                    try {
                        commands.add(cl.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        log.put(e.getMessage());

                        e.printStackTrace();
                    }
                });
    }

    public String[] keywords;
    public boolean requiresAuth;
    public boolean canRunPrivately;
    public int[] serverParameterRange;
    public CommandGroup group;
    public EmbedBuilder helpEmbed;

    public CommandBase(String keyword, boolean requiresAuth, boolean canRunPrivately, int[] parameterRange, CommandGroup group, EmbedBuilder helpEmbed) {
        this(new String[]{keyword}, requiresAuth, canRunPrivately, parameterRange, group, helpEmbed);
    }

    public CommandBase(String[] keywords, boolean requiresAuth, boolean canRunPrivately, int[] parameterRange, CommandGroup group, EmbedBuilder helpEmbed) {
        this.keywords = keywords;
        this.requiresAuth = requiresAuth;
        this.canRunPrivately = canRunPrivately;
        this.serverParameterRange = parameterRange;
        this.group = group;
        this.helpEmbed = helpEmbed;
    }

    public static void init(Package commandPackage) {
        Reflections botCommands = new Reflections(commandPackage.getName());

        botCommands
                .getSubTypesOf(CommandBase.class)
                .forEach(cl -> {
                    try {
                        commands.add(cl.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        log.put(e.getMessage());

                        e.printStackTrace();
                    }
                });
    }

    public static void process(MessageCreateEvent event) {
        Message msg = event.getMessage();
        msg.getUserAuthor().ifPresent(usr -> {
            List<String> parts = Collections.unmodifiableList(Arrays.asList(msg.getContent().split(" ")));
            List<String> param = extractParam(msg);

            if (parts.size() >= 2) {
                if (DangoBot.KEYWORDS.stream()
                        .map(String::toLowerCase)
                        .anyMatch(w -> w.equals(parts.get(0)
                                        .toLowerCase()
                                )
                        )) {

                    Optional<CommandBase> cmdOpt = findCommand(parts.get(1));

                    if (cmdOpt.isPresent()) {
                        CommandBase cmd = cmdOpt.get();

                        if (msg.isPrivate()) {
                            if (cmd.canRunPrivately) {
                                cmd.runPrivate(event, Collections.unmodifiableList(param))
                                        .evaluateForMessage(msg);
                            } else {
                                msg.getUserAuthor().ifPresent(user -> user
                                        .openPrivateChannel()
                                        .thenAcceptAsync(SuccessState.ERRORED::cantRunPrivate));
                            }
                        } else {
                            if (event.getServer().isPresent()) {
                                if (param.size() >= cmd.serverParameterRange[0] && param.size() <= cmd.serverParameterRange[1]) {
                                    Server srv = event.getServer().get();
                                    Auth auth = Auth.softGet(srv);

                                    if (!cmd.requiresAuth || auth.isAuth(usr)) {
                                        cmd.runServer(event, Collections.unmodifiableList(param))
                                                .evaluateForMessage(msg);
                                    }
                                }
                            } else {
                                SuccessState.ERRORED
                                        .withMessage("Internal Error")
                                        .evaluateForMessage(msg);
                            }
                        }
                    }
                }
            } else if (parts.size() == 1) {
                if (DangoBot.KEYWORDS.stream()
                        .map(String::toLowerCase)
                        .anyMatch(w -> w.equals(parts.get(0)
                                        .toLowerCase()
                                )
                        )) {
                    if (msg.isPrivate()) {
                        new Help().runPrivate(event, new ArrayList<>());
                    } else {
                        new Help().runServer(event, new ArrayList<>());
                    }
                }
            }
        });
    }

    public static boolean isCommand(MessageCreateEvent event) {
        Message msg = event.getMessage();
        List<String> parts = Collections.unmodifiableList(Arrays.asList(msg.getContent().split(" ")));

        if (parts.size() >= 2) {
            if (DangoBot.KEYWORDS.stream()
                    .map(String::toLowerCase)
                    .anyMatch(w -> w.equals(parts.get(0)
                                    .toLowerCase()
                            )
                    )) {

                Optional<CommandBase> cmdOpt = findCommand(parts.get(1));

                return cmdOpt.isPresent();
            }
        }

        return false;
    }

    public static Optional<CommandBase> findCommand(String keyword) {
        return commands
                .stream()
                .filter(c -> Arrays.stream(c.keywords)
                        .map(String::toLowerCase)
                        .anyMatch(w -> w.equals(keyword.toLowerCase())))
                .findAny();
    }

    private static List<String> extractParam(Message msg) {
        List<String> param, parts;

        parts = Collections.unmodifiableList(Arrays.asList(msg.getContent().split(" ")));

        if (DangoBot.KEYWORDS
                .contains(parts.get(0)
                        .toLowerCase()) &&
                parts.size() > 2) {
            param = Collections.unmodifiableList(parts.subList(2, parts.size()));
        } else {
            param = new ArrayList<>();
        }

        return param;
    }

    public abstract SuccessState runServer(MessageCreateEvent event, List<String> param);

    public abstract SuccessState runPrivate(MessageCreateEvent event, List<String> param);
}
