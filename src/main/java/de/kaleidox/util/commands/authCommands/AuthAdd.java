package de.kaleidox.util.commands.authCommands;

import de.kaleidox.util.Auth;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AuthAdd extends CommandBase {
    public AuthAdd() {
        super(new String[]{"auth", "auth-create", "auth-add"}, true, false, new int[]{0, 1}, CommandGroup.AUTH_COMMANDS,
                EmbedMaker.getBasicEmbed()
                        .addField("Auth Setup", "" +
                                "Adds one or more Users to this Server's Auth list.\n" +
                                "Auth Users have access to Bot Setup Commands.\n")
                        .addField("Note:", "_Administrators and Users with Permission \"Manage Server\" are Auth by Default._")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        List<User> mentionedUsers = msg.getMentionedUsers();
        Auth auth = Auth.softGet(srv);

        if (mentionedUsers.size() < 1) {
            returnValue.set(SuccessState.ERRORED
                    .withMessage("No User Mentions found.")
            );
        } else {
            mentionedUsers.forEach(user -> auth.addAuth(user).evaluateForMessage(msg));

            returnValue.set(SuccessState.SUCCESSFUL);
        }

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return SuccessState.ERRORED.cantRunPrivate(event.getPrivateChannel().get());
    }
}
