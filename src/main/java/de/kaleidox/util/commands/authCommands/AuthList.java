package de.kaleidox.util.commands.authCommands;

import de.kaleidox.util.Auth;
import de.kaleidox.util.SuccessState;
import de.kaleidox.util.commands.CommandBase;
import de.kaleidox.util.commands.CommandGroup;
import de.kaleidox.util.commands.EmbedMaker;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.server.Server;
import org.javacord.api.event.message.MessageCreateEvent;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AuthList extends CommandBase {
    public AuthList() {
        super(new String[]{"auths", "auth-list", "authed"}, false, false, new int[]{0, 1}, CommandGroup.AUTH_COMMANDS,
                EmbedMaker.getBasicEmbed()
                        .addField("Auth Setup", "" +
                                "Sends a list of Authed users from this Server.")
        );
    }

    @Override
    public SuccessState runServer(MessageCreateEvent event, List<String> param) {
        AtomicReference<SuccessState> returnValue = new AtomicReference<>(SuccessState.NOT_RUN);
        Message msg = event.getMessage();
        Server srv = msg.getServer().get();
        Auth auth = Auth.softGet(srv);

        msg.getServerTextChannel().ifPresent(chl -> {
            auth.sendEmbed(chl).evaluateForMessage(msg);

            returnValue.set(SuccessState.SUCCESSFUL);
        });

        return returnValue.get();
    }

    @Override
    public SuccessState runPrivate(MessageCreateEvent event, List<String> param) {
        return SuccessState.UNIMPLEMENTED;
    }
}
