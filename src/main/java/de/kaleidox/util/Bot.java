package de.kaleidox.util;

import de.kaleidox.Main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Bot {
    public static boolean isTesting() {
        return System.getProperty("os.name").equals("Windows 10");
    }

    public static String dblBotToken() {
        return readFilePlain("keys/tokenDiscordBotsOrg.txt");
    }

    public static String botToken() {
        return readFilePlain(isTesting() ? "keys/tokenTest.txt" : "keys/tokenMain.txt");
    }

    public static String botName() {
        return Main.SELF.getName();
    }

    public static String ownerTag() {
        return Main.API.getOwner().join().getDiscriminatedName();
    }

    public static String discordInvite() {
        return "http://discord.kaleidox.de";
    }

    public static String readFilePlain(String name) {
        String give;

        try {
            BufferedReader br = new BufferedReader(new FileReader(name));

            give = br.readLine();
        } catch (IOException e) {
            return "0";
        }

        return give;
    }
}
