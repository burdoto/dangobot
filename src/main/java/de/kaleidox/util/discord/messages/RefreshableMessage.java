package de.kaleidox.util.discord.messages;

import de.kaleidox.util.Emoji;
import de.kaleidox.util.listeners.MessageListeners;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.Messageable;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.listener.message.reaction.ReactionAddListener;
import org.javacord.api.listener.message.reaction.ReactionRemoveListener;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

public class RefreshableMessage {
    private final static ConcurrentHashMap<Messageable, RefreshableMessage> selfMap = new ConcurrentHashMap<>();
    private final static String REFRESH_EMOJI = "\uD83D\uDD04";

    private Messageable parent;
    private Supplier<Object> refresher;

    private Message lastMessage = null;
    private final ReactionAddListener refreshListenerAdd = event -> {
        if (!event.getUser().isYourself()) {
            Emoji emoji = new Emoji(event.getEmoji());

            if (emoji.getPrintable().equals(REFRESH_EMOJI)) {
                this.refresh();
            }
        }
    };
    private final ReactionRemoveListener refreshListenerRem = event -> {
        if (!event.getUser().isYourself()) {
            Emoji emoji = new Emoji(event.getEmoji());

            if (emoji.getPrintable().equals(REFRESH_EMOJI)) {
                this.refresh();
            }
        }
    };

    private RefreshableMessage(Messageable inParent, Supplier<Object> refresher) {
        this.parent = inParent;
        this.refresher = refresher;

        Object item = refresher.get();
        CompletableFuture<Message> sent = null;

        if (item instanceof EmbedBuilder) {
            sent = parent.sendMessage((EmbedBuilder) item);
        } else if (item instanceof String) {
            sent = parent.sendMessage((String) item);
        } else if (item instanceof File) {
            sent = parent.sendMessage((File) item);
        }

        if (sent != null) {
            sent.thenAcceptAsync(msg -> {
                lastMessage = msg;
                msg.addMessageAttachableListener(MessageListeners.MESSAGE_DELETE_CLEANUP);
                msg.addMessageAttachableListener(refreshListenerAdd);
                msg.addMessageAttachableListener(refreshListenerRem);
                msg.addReaction(REFRESH_EMOJI);
            });
        }
    }

    public final static RefreshableMessage get(Messageable forParent, Supplier<Object> defaultRefresher) {
        if (selfMap.containsKey(forParent)) {
            RefreshableMessage val = selfMap.get(forParent);
            val.resend();

            return val;
        } else {
            return selfMap.put(forParent,
                    new RefreshableMessage(
                            forParent,
                            defaultRefresher
                    )
            );
        }
    }

    public final static Optional<RefreshableMessage> get(Messageable forParent) {
        if (selfMap.containsKey(forParent)) {
            RefreshableMessage val = selfMap.get(forParent);
            val.resend();

            return Optional.of(val);
        } else return Optional.empty();
    }

    public void refresh() {
        if (lastMessage != null) {
            Object item = refresher.get();

            if (item instanceof EmbedBuilder) {
                lastMessage.edit((EmbedBuilder) item);
            } else if (item instanceof String) {
                lastMessage.edit((String) item);
            } else if (item instanceof File) {
                resend();
            }
        }
    }

    public void resend() {
        Object item = refresher.get();
        CompletableFuture<Message> sent = null;

        if (lastMessage != null) {
            lastMessage.delete("Outdated");
        }

        if (item instanceof EmbedBuilder) {
            sent = parent.sendMessage((EmbedBuilder) item);
        } else if (item instanceof String) {
            sent = parent.sendMessage((String) item);
        } else if (item instanceof File) {
            sent = parent.sendMessage((File) item);
        }

        if (sent != null) {
            sent.thenAcceptAsync(msg -> {
                lastMessage = msg;
                msg.addMessageAttachableListener(MessageListeners.MESSAGE_DELETE_CLEANUP);
                msg.addMessageAttachableListener(refreshListenerAdd);
                msg.addMessageAttachableListener(refreshListenerRem);
                msg.addReaction(REFRESH_EMOJI);
            });
        }
    }
}
