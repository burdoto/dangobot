package de.kaleidox.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import de.kaleidox.Main;

public class ExceptionReporter extends AppenderBase<ILoggingEvent> {
    @Override
    protected void append(ILoggingEvent event) {
        if (!Bot.isTesting()) {
            if (event.getLevel().levelInt >= Level.ERROR.levelInt) {
                Main.OWNER.sendMessage(new StringBuilder()
                        .append("An error occurred: ```")
                        .append(event.getThrowableProxy().getCause())
                        .append("```\n")
                        .append("")
                        .append(event.getFormattedMessage())
                        .append("\n")
                        .append("```")
                        .append(Utils.concatStrings("\n", event.getThrowableProxy().getStackTraceElementProxyArray()))
                        .append("```")
                        .toString());
            }
        }
    }
}